<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department;
use App\Status;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;



// full name is "App\Http\Controllers\UsersController"; 
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $users = User::all();
        return view('users.index', compact('users'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('admin'))
        {
            $user = User::findOrFail($id);
            $user->delete(); 
            return redirect('users'); 
        }else{
            Session::flash('notallowed', 'You are not allowed to delete the user becuase you are not admin');
        }
        return back();
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        return view('users.show', compact('user', 'departments'));
    }

    public function changeDepartment($uid, $did)
    {
        $usertochange = User::findOrFail($uid);
        $department = Department::findOrFail($did);
        if(Gate::allows('change-department', $usertochange))
        {
            $usertochange->department_id = $department->id;
            $usertochange->save();
        }else{
            Session::flash('notallowed', 'You are not allowed to change the depatrment of the user becuase the user has candidates');
        }
        return back();
        //return redirect('candidates');
    }   
    public function MakeManager($uid)
    {
        $user = User::findOrFail($uid);
        if(Gate::allows('make-manager', $user))
        {
            $role = Role::findOrFail(2);
            $user->roles()->attach($role);
        }else{
            Session::flash('notallowed', 'You are not allowed to make manager ');
        }
        return back();
        //return redirect('candidates');
    }  
    
    public function DeleteManager($uid)
    {
        $user = User::findOrFail($uid);
        if(Gate::allows('make-manager', $user))
        {
              
            $role = Role::findOrFail(2);
            $user->roles()->detach($role);
        }else{
            Session::flash('notallowed', 'You are not allowed to change the role ');
        }
        return back();
        //return redirect('candidates');
    }   
    
}
