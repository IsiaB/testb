@extends('layouts.app')

@section('title', 'User')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            @if (Auth::user()->can('admin'))
            <td>
                <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Change department
                            </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    
                        @foreach($departments as $department)
                            <a class="dropdown-item" href="{{route('users.changedepartment',['uid' => $user->id, 'did' => $department->id])}}">{{$department->name}}</a>
                            
                        @endforeach                               
                    </div>
                </div>
            </td>
            @else
                <td>{{$user->department->name}}</td>                        
            @endif
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>    
        </tr>
</table>
@endsection

