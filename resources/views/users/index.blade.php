@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Roles</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>                 
            <td>
                @foreach($user->roles as $role)
                    {{$role->name}}
                @endforeach
            </td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>    
            <td>
                <a href = "{{route('user.show',$user->id)}}">Details</a>
            </td>    
            <td>
                <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>
            <td>
            @if($user->isManager())
            <a href = "{{route('user.deletemanager', $user->id)}}">Delete manger</a>
            @else
            <a href = "{{route('user.makemanager', $user->id)}}">Make manger</a>
            @endif        
            </td>

    @endforeach
</table>
@endsection

